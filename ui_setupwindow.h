/********************************************************************************
** Form generated from reading ui file 'setupwindow.ui'
**
** Created: Mon Oct 19 19:23:20 2009
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_SETUPWINDOW_H
#define UI_SETUPWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SetupWindow
{
public:
    QAction *actionAbout;
    QAction *actionExit;
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QListWidget *lstAnimes;
    QLineEdit *txtNewAnime;
    QPushButton *btnAdd;
    QPushButton *btnRemove;
    QGroupBox *groupBox_2;
    QSpinBox *sbVotes;
    QCheckBox *cbDuplicateVotes;
    QLineEdit *txtPassword;
    QLabel *label;
    QLabel *label_2;
    QPushButton *btnStart;
    QPushButton *btnDefaults;
    QMenuBar *menuBar;
    QMenu *menuHelp;
    QMenu *menuFile;

    void setupUi(QMainWindow *SetupWindow)
    {
        if (SetupWindow->objectName().isEmpty())
            SetupWindow->setObjectName(QString::fromUtf8("SetupWindow"));
        SetupWindow->resize(362, 269);
        actionAbout = new QAction(SetupWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionExit = new QAction(SetupWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        centralWidget = new QWidget(SetupWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 0, 181, 231));
        lstAnimes = new QListWidget(groupBox);
        lstAnimes->setObjectName(QString::fromUtf8("lstAnimes"));
        lstAnimes->setGeometry(QRect(10, 40, 141, 181));
        lstAnimes->setSortingEnabled(true);
        txtNewAnime = new QLineEdit(groupBox);
        txtNewAnime->setObjectName(QString::fromUtf8("txtNewAnime"));
        txtNewAnime->setGeometry(QRect(10, 20, 141, 20));
        btnAdd = new QPushButton(groupBox);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));
        btnAdd->setGeometry(QRect(150, 20, 21, 21));
        btnRemove = new QPushButton(groupBox);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));
        btnRemove->setGeometry(QRect(150, 40, 21, 21));
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(200, 0, 151, 141));
        groupBox_2->setFlat(false);
        groupBox_2->setCheckable(false);
        sbVotes = new QSpinBox(groupBox_2);
        sbVotes->setObjectName(QString::fromUtf8("sbVotes"));
        sbVotes->setGeometry(QRect(10, 60, 131, 22));
        sbVotes->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        sbVotes->setMinimum(1);
        sbVotes->setMaximum(20);
        sbVotes->setValue(2);
        cbDuplicateVotes = new QCheckBox(groupBox_2);
        cbDuplicateVotes->setObjectName(QString::fromUtf8("cbDuplicateVotes"));
        cbDuplicateVotes->setGeometry(QRect(10, 90, 131, 18));
        cbDuplicateVotes->setChecked(false);
        txtPassword = new QLineEdit(groupBox_2);
        txtPassword->setObjectName(QString::fromUtf8("txtPassword"));
        txtPassword->setGeometry(QRect(10, 30, 131, 20));
        txtPassword->setMaxLength(20);
        txtPassword->setEchoMode(QLineEdit::Password);
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 131, 20));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 110, 141, 16));
        btnStart = new QPushButton(centralWidget);
        btnStart->setObjectName(QString::fromUtf8("btnStart"));
        btnStart->setGeometry(QRect(200, 180, 151, 23));
        btnDefaults = new QPushButton(centralWidget);
        btnDefaults->setObjectName(QString::fromUtf8("btnDefaults"));
        btnDefaults->setGeometry(QRect(200, 210, 75, 23));
        SetupWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(SetupWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 362, 22));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        SetupWindow->setMenuBar(menuBar);
        QWidget::setTabOrder(txtNewAnime, btnAdd);
        QWidget::setTabOrder(btnAdd, lstAnimes);
        QWidget::setTabOrder(lstAnimes, btnRemove);
        QWidget::setTabOrder(btnRemove, txtPassword);
        QWidget::setTabOrder(txtPassword, sbVotes);
        QWidget::setTabOrder(sbVotes, cbDuplicateVotes);
        QWidget::setTabOrder(cbDuplicateVotes, btnStart);
        QWidget::setTabOrder(btnStart, btnDefaults);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuHelp->addAction(actionAbout);
        menuFile->addAction(actionExit);

        retranslateUi(SetupWindow);
        QObject::connect(btnAdd, SIGNAL(clicked()), SetupWindow, SLOT(addAnime()));
        QObject::connect(txtNewAnime, SIGNAL(returnPressed()), SetupWindow, SLOT(addAnime()));
        QObject::connect(btnRemove, SIGNAL(clicked()), SetupWindow, SLOT(removeAnime()));
        QObject::connect(lstAnimes, SIGNAL(doubleClicked(QModelIndex)), SetupWindow, SLOT(removeAnime()));
        QObject::connect(btnDefaults, SIGNAL(clicked()), SetupWindow, SLOT(defaults()));
        QObject::connect(btnStart, SIGNAL(clicked()), SetupWindow, SLOT(startVoting()));
        QObject::connect(actionAbout, SIGNAL(triggered()), SetupWindow, SLOT(showAbout()));
        QObject::connect(actionExit, SIGNAL(triggered()), SetupWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(SetupWindow);
    } // setupUi

    void retranslateUi(QMainWindow *SetupWindow)
    {
        SetupWindow->setWindowTitle(QApplication::translate("SetupWindow", "Otakudon Voter Beta 1 - Setup", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("SetupWindow", "About", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("SetupWindow", "Exit", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("SetupWindow", "Nominations", 0, QApplication::UnicodeUTF8));
        btnAdd->setText(QApplication::translate("SetupWindow", "+", 0, QApplication::UnicodeUTF8));
        btnRemove->setText(QApplication::translate("SetupWindow", "-", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("SetupWindow", "Options", 0, QApplication::UnicodeUTF8));
        sbVotes->setPrefix(QApplication::translate("SetupWindow", "Votes per person: ", 0, QApplication::UnicodeUTF8));
        cbDuplicateVotes->setText(QApplication::translate("SetupWindow", "Duplicate votes", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SetupWindow", "Password to end voting:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("SetupWindow", "(can vote same thing twice)", 0, QApplication::UnicodeUTF8));
        btnStart->setText(QApplication::translate("SetupWindow", "Start Voting", 0, QApplication::UnicodeUTF8));
        btnDefaults->setText(QApplication::translate("SetupWindow", "Defaults", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("SetupWindow", "Help", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("SetupWindow", "File", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SetupWindow: public Ui_SetupWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETUPWINDOW_H
