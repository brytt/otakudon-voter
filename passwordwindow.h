#ifndef PASSWORDWINDOW_H
#define PASSWORDWINDOW_H

#include <QtGui/QDialog>
#include "votingwindow.h"

namespace Ui {
    class PasswordWindow;
}

class PasswordWindow : public QDialog {
    Q_OBJECT
public:
    PasswordWindow(QWidget *parent = 0);
    ~PasswordWindow();
    void takeData(QString password);

public slots:
    void submit();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::PasswordWindow *m_ui;
    VotingWindow *parent;
    QString password;
};

#endif // PASSWORDWINDOW_H
