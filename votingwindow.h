#ifndef VOTINGWINDOW_H
#define VOTINGWINDOW_H

#include <QtGui/QWidget>

namespace Ui {
    class VotingWindow;
}

class VotingWindow : public QWidget {
    Q_OBJECT
public:
    VotingWindow(QWidget *parent = 0);
    ~VotingWindow();
    void takeData(QString* list,int count,QString password,int votesPerPerson,bool duplicateVotes);
    void votingEnded();

public slots:
    void startVote();
    void endVoting();
    void addSelection();
    void removeSelection();
    void finishVote();
    void endVote();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::VotingWindow *m_ui;
    QString* list;
    int count;
    int* votes;
    QString password;
    int votesPerPerson;
    bool duplicateVotes;

    QString currentUser;
    int currentUserVotes;
};

#endif // VOTINGWINDOW_H
