#ifndef SETUPWINDOW_H
#define SETUPWINDOW_H

#include <QtGui/QMainWindow>

namespace Ui
{
    class SetupWindow;
}

class SetupWindow : public QMainWindow
{
    Q_OBJECT

public:
    SetupWindow(QWidget *parent = 0);
    ~SetupWindow();

public slots:
    void addAnime();
    void removeAnime();
    void defaults();
    void showAbout();
    void startVoting();

private:
    Ui::SetupWindow *ui;
};

#endif // SETUPWINDOW_H
