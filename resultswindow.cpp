#include "resultswindow.h"
#include "ui_resultswindow.h"
#include <stdlib.h>

ResultsWindow::ResultsWindow(QWidget *parent) :
    QWidget(parent),
    m_ui(new Ui::ResultsWindow)
{
    m_ui->setupUi(this);
}

ResultsWindow::~ResultsWindow()
{
    delete m_ui;
}

void ResultsWindow::takeData(QString *list,int count,int *votes)  {
    m_ui->tblResults->setRowCount(count);
    for(int i=0;i<count;i++)   {
        char *buffer = new char[5];
        QString v = itoa(votes[i],buffer,10);
        QTableWidgetItem* vote = new QTableWidgetItem(v);
        QTableWidgetItem* name = new QTableWidgetItem(list[i]);
        m_ui->tblResults->setItem(i,0,vote);
        m_ui->tblResults->setItem(i,1,name);
        delete buffer;
    }
    m_ui->tblResults->setSortingEnabled(true);
    m_ui->tblResults->sortByColumn(0,Qt::DescendingOrder);
    QTableWidgetItem* header1 = new QTableWidgetItem("Votes");
    QTableWidgetItem* header2 = new QTableWidgetItem("Anime");
    m_ui->tblResults->setHorizontalHeaderItem(0,header1);
    m_ui->tblResults->setHorizontalHeaderItem(1,header2);
}

void ResultsWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
