/********************************************************************************
** Form generated from reading ui file 'votewindow.ui'
**
** Created: Mon Oct 19 14:23:56 2009
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_VOTEWINDOW_H
#define UI_VOTEWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_VoteWindow
{
public:
    QGroupBox *groupBox;
    QLabel *label;
    QLineEdit *txtVoterID;
    QPushButton *btnGo;
    QPushButton *btnEnd;

    void setupUi(QWidget *VoteWindow)
    {
        if (VoteWindow->objectName().isEmpty())
            VoteWindow->setObjectName(QString::fromUtf8("VoteWindow"));
        VoteWindow->resize(237, 82);
        groupBox = new QGroupBox(VoteWindow);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 0, 211, 51));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 20, 41, 16));
        txtVoterID = new QLineEdit(groupBox);
        txtVoterID->setObjectName(QString::fromUtf8("txtVoterID"));
        txtVoterID->setGeometry(QRect(50, 20, 113, 20));
        btnGo = new QPushButton(groupBox);
        btnGo->setObjectName(QString::fromUtf8("btnGo"));
        btnGo->setGeometry(QRect(160, 20, 41, 23));
        btnEnd = new QPushButton(VoteWindow);
        btnEnd->setObjectName(QString::fromUtf8("btnEnd"));
        btnEnd->setGeometry(QRect(70, 50, 75, 23));

        retranslateUi(VoteWindow);

        QMetaObject::connectSlotsByName(VoteWindow);
    } // setupUi

    void retranslateUi(QWidget *VoteWindow)
    {
        VoteWindow->setWindowTitle(QApplication::translate("VoteWindow", "Form", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("VoteWindow", "Vote", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("VoteWindow", "Voter id:", 0, QApplication::UnicodeUTF8));
        btnGo->setText(QApplication::translate("VoteWindow", "Go", 0, QApplication::UnicodeUTF8));
        btnEnd->setText(QApplication::translate("VoteWindow", "End Voting", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(VoteWindow);
    } // retranslateUi

};

namespace Ui {
    class VoteWindow: public Ui_VoteWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VOTEWINDOW_H
