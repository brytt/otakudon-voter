#ifndef RESULTSWINDOW_H
#define RESULTSWINDOW_H

#include <QtGui/QWidget>

namespace Ui {
    class ResultsWindow;
}

class ResultsWindow : public QWidget {
    Q_OBJECT
public:
    ResultsWindow(QWidget *parent = 0);
    ~ResultsWindow();
    void takeData(QString* list,int count,int* votes);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ResultsWindow *m_ui;
};

#endif // RESULTSWINDOW_H
