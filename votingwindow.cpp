#include "votingwindow.h"
#include "ui_votingwindow.h"
#include "resultswindow.h"
#include "passwordwindow.h"

VotingWindow::VotingWindow(QWidget *parent) :
    QWidget(parent),
    m_ui(new Ui::VotingWindow)
{
    m_ui->setupUi(this);
}

VotingWindow::~VotingWindow()
{
    delete m_ui;
}

void VotingWindow::takeData(QString* list,int count,QString password,int votesPerPerson,bool duplicateVotes) {
    this->list = list;
    this->count = count;
    this->password = password;
    this->votesPerPerson = votesPerPerson;
    this->duplicateVotes = duplicateVotes;
    votes = new int[count];
    for(int j=0;j<count;j++)
        votes[j] = 0;
}
void VotingWindow::startVote()    {
    //determine if user id is good
    //TODO: make this read from a data file
    if(m_ui->txtVoterID->text() != "")  {
        currentUser = m_ui->txtVoterID->text();
        //m_ui->lstVoters->addItem(currentUser);
        m_ui->boxID->setEnabled(false);
        m_ui->boxSelect->setEnabled(true);
        for(int i=0;i<count;i++)
            m_ui->lstSelection->addItem(list[i]);
        currentUserVotes = 0;
        m_ui->lstSelection->setFocus();
    }
}
void VotingWindow::endVoting()  {
    PasswordWindow* passwindow;
    passwindow = new PasswordWindow(this);
    passwindow->show();
    passwindow->takeData(password);
}
void VotingWindow::addSelection(){
    if(currentUserVotes < votesPerPerson)   {
        m_ui->lstChoices->addItem(m_ui->lstSelection->currentItem()->text());
        if(!duplicateVotes)
            delete m_ui->lstSelection->currentItem();
        currentUserVotes++;
    }
}
void VotingWindow::removeSelection(){
    if(currentUserVotes > 0)    {
        if(!duplicateVotes)
            m_ui->lstSelection->addItem(m_ui->lstChoices->currentItem()->text());
        delete m_ui->lstChoices->currentItem();
        currentUserVotes--;
    }
}
void VotingWindow::finishVote(){
    int i = 0;
    m_ui->lstVoters->addItem(currentUser);
    while(m_ui->lstChoices->count() > 0)   {
        if(list[i] == m_ui->lstChoices->item(0)->text())    {
            votes[i]++;
            delete m_ui->lstChoices->item(0);
        }
        else
            i++;
    }
    endVote();
}
void VotingWindow::endVote(){
    currentUser = "";
    m_ui->txtVoterID->clear();
    m_ui->lstChoices->clear();
    m_ui->lstSelection->clear();
    m_ui->boxSelect->setEnabled(false);
    m_ui->boxID->setEnabled(true);
    m_ui->txtVoterID->setFocus();
}

void VotingWindow::votingEnded()    {
    ResultsWindow* results = new ResultsWindow;
    results->takeData(list,count,votes);
    results->show();
    this->close();
}

void VotingWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
