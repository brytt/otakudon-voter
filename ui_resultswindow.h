/********************************************************************************
** Form generated from reading ui file 'resultswindow.ui'
**
** Created: Thu Oct 22 16:48:11 2009
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_RESULTSWINDOW_H
#define UI_RESULTSWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ResultsWindow
{
public:
    QTableWidget *tblResults;
    QPushButton *btnDone;

    void setupUi(QWidget *ResultsWindow)
    {
        if (ResultsWindow->objectName().isEmpty())
            ResultsWindow->setObjectName(QString::fromUtf8("ResultsWindow"));
        ResultsWindow->resize(304, 511);
        tblResults = new QTableWidget(ResultsWindow);
        if (tblResults->columnCount() < 2)
            tblResults->setColumnCount(2);
        tblResults->setObjectName(QString::fromUtf8("tblResults"));
        tblResults->setGeometry(QRect(10, 10, 281, 461));
        tblResults->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tblResults->setRowCount(0);
        tblResults->setColumnCount(2);
        tblResults->horizontalHeader()->setVisible(true);
        tblResults->horizontalHeader()->setDefaultSectionSize(40);
        tblResults->horizontalHeader()->setStretchLastSection(true);
        tblResults->verticalHeader()->setVisible(false);
        tblResults->verticalHeader()->setStretchLastSection(false);
        btnDone = new QPushButton(ResultsWindow);
        btnDone->setObjectName(QString::fromUtf8("btnDone"));
        btnDone->setEnabled(false);
        btnDone->setGeometry(QRect(10, 480, 281, 23));

        retranslateUi(ResultsWindow);

        QMetaObject::connectSlotsByName(ResultsWindow);
    } // setupUi

    void retranslateUi(QWidget *ResultsWindow)
    {
        ResultsWindow->setWindowTitle(QApplication::translate("ResultsWindow", "Otakudon Voter Beta 1 - Results", 0, QApplication::UnicodeUTF8));
        btnDone->setText(QApplication::translate("ResultsWindow", "Done", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(ResultsWindow);
    } // retranslateUi

};

namespace Ui {
    class ResultsWindow: public Ui_ResultsWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESULTSWINDOW_H
