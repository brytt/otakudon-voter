/********************************************************************************
** Form generated from reading ui file 'votingwindow.ui'
**
** Created: Thu Oct 22 21:58:39 2009
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_VOTINGWINDOW_H
#define UI_VOTINGWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_VotingWindow
{
public:
    QGroupBox *boxSelect;
    QListWidget *lstSelection;
    QPushButton *btnAdd;
    QPushButton *btnRemove;
    QPushButton *btnFinish;
    QPushButton *btnCancel;
    QListWidget *lstChoices;
    QGroupBox *boxID;
    QLabel *label;
    QLineEdit *txtVoterID;
    QPushButton *btnGo;
    QGroupBox *groupBox;
    QListWidget *lstVoters;
    QPushButton *btnEnd;

    void setupUi(QWidget *VotingWindow)
    {
        if (VotingWindow->objectName().isEmpty())
            VotingWindow->setObjectName(QString::fromUtf8("VotingWindow"));
        VotingWindow->resize(514, 327);
        boxSelect = new QGroupBox(VotingWindow);
        boxSelect->setObjectName(QString::fromUtf8("boxSelect"));
        boxSelect->setEnabled(false);
        boxSelect->setGeometry(QRect(10, 70, 341, 241));
        lstSelection = new QListWidget(boxSelect);
        lstSelection->setObjectName(QString::fromUtf8("lstSelection"));
        lstSelection->setGeometry(QRect(10, 20, 151, 191));
        lstSelection->setSortingEnabled(true);
        btnAdd = new QPushButton(boxSelect);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));
        btnAdd->setGeometry(QRect(160, 20, 21, 21));
        btnRemove = new QPushButton(boxSelect);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));
        btnRemove->setGeometry(QRect(160, 40, 21, 21));
        btnFinish = new QPushButton(boxSelect);
        btnFinish->setObjectName(QString::fromUtf8("btnFinish"));
        btnFinish->setGeometry(QRect(180, 210, 81, 23));
        btnCancel = new QPushButton(boxSelect);
        btnCancel->setObjectName(QString::fromUtf8("btnCancel"));
        btnCancel->setGeometry(QRect(260, 210, 75, 23));
        lstChoices = new QListWidget(boxSelect);
        lstChoices->setObjectName(QString::fromUtf8("lstChoices"));
        lstChoices->setGeometry(QRect(180, 20, 151, 192));
        lstChoices->setSortingEnabled(true);
        boxID = new QGroupBox(VotingWindow);
        boxID->setObjectName(QString::fromUtf8("boxID"));
        boxID->setGeometry(QRect(10, 20, 341, 51));
        label = new QLabel(boxID);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 20, 41, 21));
        txtVoterID = new QLineEdit(boxID);
        txtVoterID->setObjectName(QString::fromUtf8("txtVoterID"));
        txtVoterID->setGeometry(QRect(60, 20, 111, 20));
        btnGo = new QPushButton(boxID);
        btnGo->setObjectName(QString::fromUtf8("btnGo"));
        btnGo->setGeometry(QRect(170, 20, 71, 21));
        groupBox = new QGroupBox(VotingWindow);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(350, 0, 151, 311));
        lstVoters = new QListWidget(groupBox);
        lstVoters->setObjectName(QString::fromUtf8("lstVoters"));
        lstVoters->setGeometry(QRect(10, 20, 131, 281));
        lstVoters->setSortingEnabled(true);
        btnEnd = new QPushButton(VotingWindow);
        btnEnd->setObjectName(QString::fromUtf8("btnEnd"));
        btnEnd->setGeometry(QRect(140, 0, 71, 21));

        retranslateUi(VotingWindow);
        QObject::connect(btnGo, SIGNAL(clicked()), VotingWindow, SLOT(startVote()));
        QObject::connect(btnEnd, SIGNAL(clicked()), VotingWindow, SLOT(endVoting()));
        QObject::connect(txtVoterID, SIGNAL(returnPressed()), VotingWindow, SLOT(startVote()));
        QObject::connect(btnAdd, SIGNAL(clicked()), VotingWindow, SLOT(addSelection()));
        QObject::connect(btnRemove, SIGNAL(clicked()), VotingWindow, SLOT(removeSelection()));
        QObject::connect(lstSelection, SIGNAL(itemDoubleClicked(QListWidgetItem*)), VotingWindow, SLOT(addSelection()));
        QObject::connect(lstChoices, SIGNAL(itemDoubleClicked(QListWidgetItem*)), VotingWindow, SLOT(removeSelection()));
        QObject::connect(btnFinish, SIGNAL(clicked()), VotingWindow, SLOT(finishVote()));
        QObject::connect(btnCancel, SIGNAL(clicked()), VotingWindow, SLOT(endVote()));

        QMetaObject::connectSlotsByName(VotingWindow);
    } // setupUi

    void retranslateUi(QWidget *VotingWindow)
    {
        VotingWindow->setWindowTitle(QApplication::translate("VotingWindow", "Otakudon Voter Beta 1 - Voting", 0, QApplication::UnicodeUTF8));
        boxSelect->setTitle(QApplication::translate("VotingWindow", "Vote Selection", 0, QApplication::UnicodeUTF8));
        btnAdd->setText(QApplication::translate("VotingWindow", "+", 0, QApplication::UnicodeUTF8));
        btnRemove->setText(QApplication::translate("VotingWindow", "-", 0, QApplication::UnicodeUTF8));
        btnFinish->setText(QApplication::translate("VotingWindow", "Finish voting", 0, QApplication::UnicodeUTF8));
        btnCancel->setText(QApplication::translate("VotingWindow", "Cancel", 0, QApplication::UnicodeUTF8));
        boxID->setTitle(QApplication::translate("VotingWindow", "Vote", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("VotingWindow", "Voter id:", 0, QApplication::UnicodeUTF8));
        btnGo->setText(QApplication::translate("VotingWindow", "Vote", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("VotingWindow", "Voters", 0, QApplication::UnicodeUTF8));
        btnEnd->setText(QApplication::translate("VotingWindow", "End Voting", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(VotingWindow);
    } // retranslateUi

};

namespace Ui {
    class VotingWindow: public Ui_VotingWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VOTINGWINDOW_H
