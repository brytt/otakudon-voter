/****************************************************************************
** Meta object code from reading C++ file 'setupwindow.h'
**
** Created: Thu Oct 22 16:48:47 2009
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../setupwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'setupwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SetupWindow[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,
      24,   12,   12,   12, 0x0a,
      38,   12,   12,   12, 0x0a,
      49,   12,   12,   12, 0x0a,
      61,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SetupWindow[] = {
    "SetupWindow\0\0addAnime()\0removeAnime()\0"
    "defaults()\0showAbout()\0startVoting()\0"
};

const QMetaObject SetupWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_SetupWindow,
      qt_meta_data_SetupWindow, 0 }
};

const QMetaObject *SetupWindow::metaObject() const
{
    return &staticMetaObject;
}

void *SetupWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SetupWindow))
        return static_cast<void*>(const_cast< SetupWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int SetupWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: addAnime(); break;
        case 1: removeAnime(); break;
        case 2: defaults(); break;
        case 3: showAbout(); break;
        case 4: startVoting(); break;
        default: ;
        }
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
