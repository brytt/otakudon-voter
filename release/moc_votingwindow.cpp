/****************************************************************************
** Meta object code from reading C++ file 'votingwindow.h'
**
** Created: Thu Oct 22 21:40:45 2009
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../votingwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'votingwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_VotingWindow[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,
      26,   13,   13,   13, 0x0a,
      38,   13,   13,   13, 0x0a,
      53,   13,   13,   13, 0x0a,
      71,   13,   13,   13, 0x0a,
      84,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_VotingWindow[] = {
    "VotingWindow\0\0startVote()\0endVoting()\0"
    "addSelection()\0removeSelection()\0"
    "finishVote()\0endVote()\0"
};

const QMetaObject VotingWindow::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VotingWindow,
      qt_meta_data_VotingWindow, 0 }
};

const QMetaObject *VotingWindow::metaObject() const
{
    return &staticMetaObject;
}

void *VotingWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VotingWindow))
        return static_cast<void*>(const_cast< VotingWindow*>(this));
    return QWidget::qt_metacast(_clname);
}

int VotingWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: startVote(); break;
        case 1: endVoting(); break;
        case 2: addSelection(); break;
        case 3: removeSelection(); break;
        case 4: finishVote(); break;
        case 5: endVote(); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
