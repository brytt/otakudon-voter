/********************************************************************************
** Form generated from reading ui file 'passwordwindow.ui'
**
** Created: Thu Oct 22 20:51:30 2009
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_PASSWORDWINDOW_H
#define UI_PASSWORDWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_PasswordWindow
{
public:
    QLineEdit *txtPassword;
    QPushButton *btnSubmit;
    QPushButton *btnCancel;

    void setupUi(QDialog *PasswordWindow)
    {
        if (PasswordWindow->objectName().isEmpty())
            PasswordWindow->setObjectName(QString::fromUtf8("PasswordWindow"));
        PasswordWindow->resize(194, 75);
        txtPassword = new QLineEdit(PasswordWindow);
        txtPassword->setObjectName(QString::fromUtf8("txtPassword"));
        txtPassword->setGeometry(QRect(10, 10, 171, 20));
        txtPassword->setMaxLength(20);
        txtPassword->setEchoMode(QLineEdit::Password);
        btnSubmit = new QPushButton(PasswordWindow);
        btnSubmit->setObjectName(QString::fromUtf8("btnSubmit"));
        btnSubmit->setGeometry(QRect(10, 40, 75, 23));
        btnCancel = new QPushButton(PasswordWindow);
        btnCancel->setObjectName(QString::fromUtf8("btnCancel"));
        btnCancel->setGeometry(QRect(100, 40, 75, 23));

        retranslateUi(PasswordWindow);
        QObject::connect(btnSubmit, SIGNAL(clicked()), PasswordWindow, SLOT(submit()));
        QObject::connect(btnCancel, SIGNAL(clicked()), PasswordWindow, SLOT(close()));
        QObject::connect(txtPassword, SIGNAL(returnPressed()), PasswordWindow, SLOT(submit()));

        QMetaObject::connectSlotsByName(PasswordWindow);
    } // setupUi

    void retranslateUi(QDialog *PasswordWindow)
    {
        PasswordWindow->setWindowTitle(QApplication::translate("PasswordWindow", "Enter password to end voting", 0, QApplication::UnicodeUTF8));
        btnSubmit->setText(QApplication::translate("PasswordWindow", "End Voting", 0, QApplication::UnicodeUTF8));
        btnCancel->setText(QApplication::translate("PasswordWindow", "Cancel", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(PasswordWindow);
    } // retranslateUi

};

namespace Ui {
    class PasswordWindow: public Ui_PasswordWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PASSWORDWINDOW_H
