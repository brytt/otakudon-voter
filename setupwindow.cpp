#include "setupwindow.h"
#include "ui_setupwindow.h"
#include "votingwindow.h"
#include <QMessageBox>

SetupWindow::SetupWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::SetupWindow)
{
    ui->setupUi(this);
}

SetupWindow::~SetupWindow()
{
    delete ui;
}

void SetupWindow::addAnime() {
    if(ui->txtNewAnime->text() != "")   {
        ui->lstAnimes->addItem(ui->txtNewAnime->text());
        ui->txtNewAnime->clear();
    }
}

void SetupWindow::removeAnime()  {
    if(ui->lstAnimes->currentRow() >= 0)
      //  ui->lstAnimes->removeItemWidget(ui->lstAnimes->currentItem());
        delete ui->lstAnimes->currentItem();
}

void SetupWindow::defaults()    {
    ui->lstAnimes->clear();
    ui->txtNewAnime->clear();
    ui->txtPassword->clear();
    ui->sbVotes->setValue(2);
    ui->cbDuplicateVotes->setChecked(false);
}

void SetupWindow::showAbout()   {
    QMessageBox msgBox;
    msgBox.setText("Otakudon Voter Beta 1\nDesigned by Bryton \"FLiB\" Greenwood for the Radford University Otaku-Don.\nNot officially released. License to be determined upon release time.\nThanks to: All of the Otaku-Don, Laura Bramble, Doctor Andrew Ray");
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.exec();
}

void SetupWindow::startVoting() {
    int count = ui->lstAnimes->count();
    QString* list = new QString[count];
    for(int i=0;i<count;i++)
        list[i] = ui->lstAnimes->item(i)->text();
    QString password = ui->txtPassword->text();
    int votesPerPerson = ui->sbVotes->value();
    bool duplicateVotes = ui->cbDuplicateVotes->isChecked();

    VotingWindow* voting = new VotingWindow;
    voting->takeData(list,count,password,votesPerPerson,duplicateVotes);
    voting->show();
    this->close();
}
