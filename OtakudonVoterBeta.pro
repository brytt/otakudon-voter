# -------------------------------------------------
# Project created by QtCreator 2009-10-19T13:38:51
# -------------------------------------------------
TARGET = OtakudonVoterBeta
TEMPLATE = app
SOURCES += main.cpp \
    setupwindow.cpp \
    votingwindow.cpp \
    resultswindow.cpp \
    passwordwindow.cpp
HEADERS += setupwindow.h \
    votingwindow.h \
    resultswindow.h \
    passwordwindow.h
FORMS += setupwindow.ui \
    votingwindow.ui \
    resultswindow.ui \
    passwordwindow.ui
