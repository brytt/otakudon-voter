#include "passwordwindow.h"
#include "ui_passwordwindow.h"

PasswordWindow::PasswordWindow(QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::PasswordWindow)
{
    this->parent = (VotingWindow*)parent;
    m_ui->setupUi(this);
}

PasswordWindow::~PasswordWindow()
{
    delete m_ui;
}

void PasswordWindow::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void PasswordWindow::takeData(QString password) {
    this->password = password;
}

void PasswordWindow::submit()   {
    if(m_ui->txtPassword->text() == password)   {
        parent->votingEnded();
        this->close();
    }
}
